DDL History jest to zewnętrzna baza danych mająca na celu zbieranie informacji strukturach innych baz dodanych do konfiguracji.

Wystarczy dodać do joba procedurę o nazwie "p_check_and_insert_all_changes" oraz wprowadzić interesującą nas konfigurację za pomocą procedury "p_add_new_database_to_configuration" i historia zmian w bazie danych będzie się sama logowała.

Dodanie konfiguracji za pomocą procedury o nazwie "p_check_and_insert_all_changes" wymaga podania przynajmniej 1 parametru - nazwy bazy danych, kolejne 4 parametry są opcjonalne i oznaczają one w kolejności, są to bitowe wartości:
- loguj zmiany w tabelach
- loguj zmiany w triggerach
- loguj zmiany w programmability
- logij zmiany w widokach

Aby zobaczyć zmiany należy wywołać procedurę o nazwie "p_show_object_history" z wymaganym parametrem nazwy bazy danych, typem objektu (U - tabela, P - procedura, FN - funkcja scalarna, TF - funkcja tabelaryczna, V - widok, TR - trigger) oraz nazwą obiektu.

Aby zmienić konfigurację należy użyć procedury o nazwie "p_change_database_configuration", przyjmującej parametry:
- Nazwa bazy danych
- flaga archiwalności
- loguj zmiany w tabelach
- loguj zmiany w triggerach
- loguj zmiany w programmability
- logij zmiany w widokach
