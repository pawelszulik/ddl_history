USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_database_configuration](
	[dconf_db_name] [varchar](128) NOT NULL,
	[dconf_is_archived] [bit] NOT NULL,
	[dconf_last_check_date] [datetime] NOT NULL,
	[dconf_is_logging_table] [bit] NOT NULL,
	[dconf_last_change_table] [datetime] NOT NULL,
	[dconf_is_logging_trigger] [bit] NOT NULL,
	[dconf_last_change_trigger] [datetime] NOT NULL,
	[dconf_is_logging_programmability] [bit] NOT NULL,
	[dconf_last_change_programmability] [datetime] NOT NULL,
	[dconf_is_logging_view] [bit] NOT NULL,
	[dconf_last_change_view] [datetime] NOT NULL,
 CONSTRAINT [PK_t_database_configuration] PRIMARY KEY CLUSTERED 
(
	[dconf_db_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_object_history](
	[obh_id] [int] IDENTITY(1,1) NOT NULL,
	[obh_db_name] [varchar](128) NOT NULL,
	[obh_db_schema] [varchar](128) NOT NULL,
	[obh_object_id] [int] NOT NULL,
	[obh_object_name] [varchar](128) NOT NULL,
	[obh_object_type] [varchar](128) NOT NULL,
	[obh_object_definition] [nvarchar](max) NOT NULL,
	[obh_object_created_datetime] [datetime] NOT NULL,
	[obh_object_altered_datetime] [datetime] NOT NULL,
	[obh_inserted_date] [datetime] NOT NULL,
 CONSTRAINT [PK_obh_id] PRIMARY KEY CLUSTERED 
(
	[obh_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_obh] ON [dbo].[t_object_history]
(
	[obh_db_name] ASC,
	[obh_db_schema] ASC,
	[obh_object_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_configuration_conf_last_check_date]  DEFAULT (sysdatetimeoffset()) FOR [dconf_last_check_date]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_table]  DEFAULT ((1)) FOR [dconf_is_logging_table]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_table]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_trigger]  DEFAULT ((1)) FOR [dconf_is_logging_trigger]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_trigger]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_stored_procedure]  DEFAULT ((1)) FOR [dconf_is_logging_programmability]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_programmability]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_view]  DEFAULT ((1)) FOR [dconf_is_logging_view]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_last_change_view]  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_view]
GO
ALTER TABLE [dbo].[t_object_history] ADD  DEFAULT (getdate()) FOR [obh_inserted_date]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE   PROCEDURE [dbo].[p_add_new_database_to_configuration] 
				 @database_name VARCHAR(128)
			   , @is_logging_table bit = 1
			   , @is_logging_trigger bit = 1
			   , @is_logging_programmability bit = 1
			   , @is_logging_view bit = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE 
		   @is_exists BIT;
	SET @is_exists = ISNULL(
		(
		SELECT 
			   1
		FROM 
			 [dbo].[t_database_configuration]
		WHERE [dconf_db_name] = @database_name
		), 0);

	IF @is_exists = 1
		BEGIN
			RAISERROR('Taka baza już istnieje!', 16, 1);
			RETURN;
		END;

	INSERT INTO [dbo].[t_database_configuration]
		   (
		   [dconf_db_name]
		   ,[dconf_is_archived]
           ,[dconf_is_logging_table]
           ,[dconf_is_logging_trigger]
           ,[dconf_is_logging_programmability]
           ,[dconf_is_logging_view]
		   )
	VALUES
		   ( 
		   @database_name
		   ,0
		   , @is_logging_table
		   , @is_logging_trigger
		   , @is_logging_programmability
		   , @is_logging_view
		   );
END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE   PROCEDURE [dbo].[p_change_database_configuration] 
				 @database_name VARCHAR(128)
			   , @is_archived bit = 0
			   , @is_logging_table bit = 1
			   , @is_logging_trigger bit = 1
			   , @is_logging_programmability bit = 1
			   , @is_logging_view bit = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE 
		   @is_exists BIT;
	SET @is_exists = ISNULL(
		(
		SELECT 
			   1
		FROM 
			 [dbo].[t_database_configuration]
		WHERE [dconf_db_name] = @database_name
		), 0);

	IF @is_exists = 0
		BEGIN
			RAISERROR('Taka baza nie istnieje!', 16, 1);
			RETURN;
		END;
		
	UPDATE 
		[dbo].[t_database_configuration]
	SET
		[dconf_is_archived] = @is_archived
		,[dconf_is_logging_table] = @is_logging_table
		,[dconf_is_logging_trigger] = @is_logging_trigger
		,[dconf_is_logging_programmability] = @is_logging_programmability
		,[dconf_is_logging_view] = @is_logging_view
	WHERE
		[dconf_db_name] = @database_name;
END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_check_and_insert_all_changes]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @database [varchar](128);
	DECLARE @is_logging_table  bit;
	DECLARE @last_change_table  DATETIME
	DECLARE @is_logging_trigger  bit;
	DECLARE @last_change_trigger  DATETIME
	DECLARE @is_logging_programmability  bit;
	DECLARE @last_change_programmability  DATETIME
	DECLARE @is_logging_view  bit;
	DECLARE @last_change_view  DATETIME
	DECLARE @rows INT;
	DECLARE @i INT = 1;
	DECLARE @current_date DATETIME = GETDATE();
	DECLARE @last_change_date DATETIME;
	DECLARE @proc_to_create NVARCHAR(MAX);
	DECLARE @proc NVARCHAR(MAX);
	DECLARE @sql NVARCHAR(MAX);
	DECLARE @type CHAR(2);
	DECLARE @local_max_date DATETIME;

	-- Pobranie wszystkich baz w konfiguracji, które nie są archiwalne
	-- Odfiltrowanie baz które są offline
	IF OBJECT_ID('tempdb..#tmp_database') IS NOT NULL DROP TABLE #tmp_database
	CREATE TABLE #tmp_database
	(
		[ordinal_number] INT NOT NULL,
		[database] [varchar](128) NOT NULL,
		[is_logging_table]  bit NOT NULL,
		[last_change_table]  DATETIME NULL,
		[is_logging_trigger]  bit NOT NULL,
		[last_change_trigger]  DATETIME NULL,
		[is_logging_programmability]  bit NOT NULL,
		[last_change_programmability]  DATETIME NULL,
		[is_logging_view]  bit NOT NULL,
		[last_change_view]  DATETIME NULL
	);
	INSERT INTO 
		#tmp_database 
		(
			[ordinal_number]
			, [database]
			, [is_logging_table]
			, [last_change_table]
			, [is_logging_trigger]
			, [last_change_trigger]
			, [is_logging_programmability]
			, [last_change_programmability]
			, [is_logging_view]
			, [last_change_view]
		)
	SELECT 
		ROW_NUMBER() OVER (order by d.database_id asc)
		, [dconf_db_name]
		  ,[dconf_is_logging_table]
		  ,[dconf_last_change_table]
		  ,[dconf_is_logging_trigger]
		  ,[dconf_last_change_trigger]
		  ,[dconf_is_logging_programmability]
		  ,[dconf_last_change_programmability]
		  ,[dconf_is_logging_view]
		  ,[dconf_last_change_view]
	FROM 
		[dbo].[t_database_configuration]
		INNER JOIN sys.databases d ON d.[name] = [dconf_db_name] AND d.[state] = 0 AND d.database_id > 4
	WHERE
		dconf_is_archived = 0;

	SELECT 
		   @rows = COUNT(1)
	FROM 
		 #tmp_database;
		 
	-- Dla każdej z baz
	WHILE @i <= @rows
		BEGIN
			SELECT 
				 @database = [database]
				, @is_logging_table = [is_logging_table]
				, @last_change_table = [last_change_table]
				, @is_logging_trigger = [is_logging_trigger]
				, @last_change_trigger = [last_change_trigger]
				, @is_logging_programmability = [is_logging_programmability]
				, @last_change_programmability = [last_change_programmability]
				, @is_logging_view = [is_logging_view]
				, @last_change_view = [last_change_view]
			FROM 
				#tmp_database
			WHERE
				[ordinal_number] = @i;
						
			-- Jezeli nie istnie procedura do wyciagania max daty modyfikacji
			IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_get_max_modify_date_' + @database))
				BEGIN				
					SELECT 
						@proc_to_create = REPLACE([ROUTINE_DEFINITION], 'set_database_name', @database)
					FROM 
						INFORMATION_SCHEMA.routines
					WHERE 
						routine_name = 'p_get_max_modify_date_set_database_name';
					-- Tworz ja
					EXEC (@proc_to_create);
				END

			-- Tabele
			IF (@is_logging_table = 1)
			BEGIN	
				-- Typ tabeli
				SET @type = 'U';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				
				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_table < @last_change_date
				BEGIN
					-- Jezeli nie istnieje procedura
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_table_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_table_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure do tworzenia skryptow
					SET @sql = CONCAT('p_table_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;
					
					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_table] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END						

			END	

			-- programmability			
			IF (@is_logging_programmability = 1)
			BEGIN	
				-- Typ
				SET @type = 'P';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				SET @local_max_date = @last_change_date;
				
				-- Typ
				SET @type = 'FN';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT

				IF @local_max_date < @last_change_date
					BEGIN
						SET @local_max_date = @last_change_date;
					END
				
				-- Typ
				SET @type = 'TF';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT

				IF @local_max_date < @last_change_date
					BEGIN
						SET @local_max_date = @last_change_date;
					END

				SET @last_change_date = @local_max_date;

				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_programmability < @last_change_date
				BEGIN
					-- Jezeli nie istnieje procedura
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_programmability_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_programmability_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure
					SET @sql = CONCAT('p_programmability_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;

					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_programmability] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END						
			END

			
			-- widok			
			IF (@is_logging_view = 1)
			BEGIN	
				-- Typ
				SET @type = 'V';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				
				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_view < @last_change_date
				BEGIN
					-- Jezeli nie istnieje tabela do tworzenia skryptow tabel
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_view_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_view_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure
					SET @sql = CONCAT('p_view_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;
					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_view] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END							
			END

			
			-- trigger			
			IF (@is_logging_trigger = 1)
			BEGIN	
				-- Typ
				SET @type = 'TR';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				
				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_trigger < @last_change_date
				BEGIN
					-- Jezeli nie istnieje tabela do tworzenia skryptow tabel
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_trigger_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_trigger_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure
					SET @sql = CONCAT('p_trigger_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;
					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_trigger] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END
			END

			SET @i = @i + 1;
		END;
	
	UPDATE
		dc
	SET
		dc.[dconf_last_check_date] = @current_date
	FROM 
		[dbo].[t_database_configuration] dc
		INNER JOIN sys.databases d ON d.[name] = dc.[dconf_db_name] AND d.[state] = 0 AND d.database_id > 4
	WHERE
		dc.dconf_is_archived = 0

END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[p_get_max_modify_date_set_database_name]
@type char(2), @max_datetime DATETIME OUTPUT
AS
BEGIN
	SET NOCOUNT ON;


	SELECT 
		   @max_datetime = MAX(all_obj.modify_date)
	FROM 
		 [set_database_name].sys.all_objects AS all_obj
	WHERE
		all_obj.type = @type;

END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[p_programmability_check_and_insert_all_changes_set_database_name]
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[t_object_history]
           ([obh_db_name]
           ,[obh_db_schema]
           ,[obh_object_id]
           ,[obh_object_name]
           ,[obh_object_type]
           ,[obh_object_definition]
           ,[obh_object_created_datetime]
           ,[obh_object_altered_datetime]
           ,[obh_inserted_date])
		SELECT 
			[conf].[dconf_db_name]
			, [schemats].[name]
			, [all_object].[object_id]
			, [all_object].[name]
			, [all_object].[type]
			, [sql_mod].[definition]
			, [all_object].[create_date] 
			, [all_object].[modify_date] 
			, GETDATE()
		FROM 
			[set_database_name].[sys].[all_objects] AS [all_object]
			INNER JOIN 
				[set_database_name].[sys].[schemas] AS [schemats]
			ON 
				[all_object].schema_id = [schemats].schema_id
			INNER JOIN [set_database_name].sys.sql_modules sql_mod 
			ON 
				[sql_mod].object_id = [all_object].object_id
			INNER JOIN 
				[dbo].[t_database_configuration] AS [conf]
			ON 
				[conf].[dconf_is_archived] = 0 
				AND 'set_database_name' = conf.dconf_db_name COLLATE DATABASE_DEFAULT
			INNER JOIN 	[set_database_name].[INFORMATION_SCHEMA].[ROUTINES] AS [programmbility]
				ON		[programmbility].[ROUTINE_CATALOG] = [conf].[dconf_db_name]  COLLATE DATABASE_DEFAULT
					AND [programmbility].[ROUTINE_SCHEMA] = [schemats].[name] COLLATE DATABASE_DEFAULT 
					AND [programmbility].[ROUTINE_NAME] = [all_object].[name] COLLATE DATABASE_DEFAULT
			LEFT JOIN [dbo].[t_object_history] AS [obj_hist]
				ON		[obj_hist].[obh_db_name] = [conf].[dconf_db_name] COLLATE DATABASE_DEFAULT
					AND [obj_hist].[obh_db_schema] = [schemats].[name] COLLATE DATABASE_DEFAULT
					AND [obj_hist].[obh_object_name] = [all_object].[name] COLLATE DATABASE_DEFAULT
			LEFT JOIN 
				(	
					SELECT 
						MAX([max_obj_hist].[obh_object_altered_datetime]) as 'maximum'
						, [max_obj_hist].[obh_db_name]
						, [max_obj_hist].[obh_db_schema]
						, [max_obj_hist].[obh_object_name]
					FROM 
						[dbo].[t_object_history] AS [max_obj_hist]
					GROUP BY
						[max_obj_hist].[obh_db_name]
						, [max_obj_hist].[obh_db_schema]
						, [max_obj_hist].[obh_object_name]
				) [max_date]
			ON 
				[max_date].[obh_db_name] = [obj_hist].[obh_db_name]
				AND [max_date].[obh_db_schema] = [obj_hist].[obh_db_schema]
				AND [max_date].[obh_object_name] = [obj_hist].[obh_object_name]
				AND [max_date].[maximum] = [obj_hist].[obh_object_altered_datetime]
			WHERE
				([obj_hist].[obh_id] IS NULL)
				OR [max_date].[maximum] < [all_object].[modify_date];
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_show_object_history] 
				 @database_name VARCHAR(128)
				 , @obect_type char(2) = null
				 , @object_name VARCHAR(128) = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[obh_db_name]						AS 'Database name'
		,[obh_db_schema]					AS 'Schema name'
		,[obh_object_id]					AS 'Object Id'
		,[obh_object_name]					AS 'Object name'
		,[obh_object_type]					AS 'Object type'
		,[obh_object_definition]			AS 'Object definition'
		,[obh_object_created_datetime]		AS 'Object create date'
		,[obh_object_altered_datetime]		AS 'Object alter date'
		,[obh_inserted_date]				AS 'Insert date'
	FROM 
		[dbo].[t_object_history]
	WHERE
		[obh_db_name] = @database_name
		AND (@obect_type IS NULL OR [obh_object_type] = @obect_type)
		AND (@object_name IS NULL OR [obh_object_name] = @object_name)
	ORDER BY
		[obh_db_name] ASC, [obh_db_schema] ASC, [obh_object_type] ASC, [obh_object_name] ASC, [obh_object_altered_datetime] DESC;

END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_table_check_and_insert_all_changes_set_database_name]
AS
BEGIN
SET NOCOUNT ON;

	DECLARE 
		   @schema_name SYSNAME
		 , @table_name  SYSNAME
		 , @full_name   SYSNAME
		 , @object_id   INT
		 , @sql         NVARCHAR(MAX)
		 , @fk          NVARCHAR(MAX);
	IF OBJECT_ID('tempdb..#TableScript') IS NOT NULL
	BEGIN
		DROP TABLE #TableScript;
	END;

	CREATE TABLE #TableScript
				 (
				 [row_id]       [INT] NOT NULL
			   , [table_name]   [VARCHAR](255) NOT NULL
			   , [schema_name]  [VARCHAR](255) NOT NULL
			   , [full_name]    [VARCHAR](255) NOT NULL
			   , [object_id]    [INT] NOT NULL
			   , [table_script] NVARCHAR(MAX) NULL
				 );

	INSERT INTO #TableScript
		   (
		   row_id
		 , [table_name]
		 , [schema_name]
		 , [full_name]
		 , object_id
		   )
		   SELECT 
				  ROW_NUMBER() OVER(
				  ORDER BY 
						   tab.object_id)
				, QUOTENAME(tab.name)
				, QUOTENAME(s.name)
				, CONCAT(QUOTENAME(s.name), '.', QUOTENAME(tab.name))
				, tab.object_id
		   FROM 
				[set_database_name].sys.tables AS tab
				INNER JOIN [set_database_name].sys.schemas AS s
					 ON tab.schema_id = s.schema_id;

	DECLARE 
		   @rows INT;
	DECLARE 
		   @i INT = 1;

	SELECT 
		   @rows = COUNT(1)
	FROM 
		 #TableScript;

	WHILE @i <= @rows
	BEGIN
		SELECT 
			   @full_name = [full_name]
			 , @object_id = [object_id]
			 , @schema_name = [schema_name]
			 , @table_name = [table_name]
		FROM 
			 #TableScript
		WHERE row_id = @i;
		WITH cteForeignKey
			 AS (SELECT 
						s.name AS        schemaName
					  , t.[name] AS      tableName
					  , sfk.[name] AS    fkName
					  , refs.name AS     refSchemaName
					  , reft.name AS     refTablename
					  , sfk.object_id AS fkObjectID
					  , sfk.is_disabled
					  , sfk.is_not_for_replication
					  , sfk.is_not_trusted
					  , sfk.delete_referential_action
					  , sfk.update_referential_action
					  , pk_cols = STUFF(
						(
						SELECT 
							   ','+refc.name
						FROM 
							 [set_database_name].sys.foreign_key_columns AS fkc
						WHERE fkc.constraint_object_id = sfk.object_id FOR
						XML PATH('')
						), 1, 1, '')
					  , fk_cols = STUFF(
						(
						SELECT 
							   ','+fparc.name
						FROM 
							 [set_database_name].sys.foreign_key_columns AS fkc
							 INNER JOIN [set_database_name].sys.columns AS fparc WITH(
							 NOLOCK)
								  ON
									 fparc.column_id = fkc.parent_column_id
									 AND fparc.object_id = fkc.
									 parent_object_id
						WHERE fkc.constraint_object_id = sfk.object_id FOR
						XML PATH('')
						), 1, 1, '')
				 FROM 
					  [set_database_name].sys.foreign_keys AS sfk
					  INNER JOIN [set_database_name].sys.tables AS t
						   ON t.object_id = sfk.parent_object_id
					  INNER JOIN [set_database_name].sys.schemas AS s
						   ON s.schema_id = t.schema_id
					  INNER JOIN [set_database_name].sys.indexes AS refi WITH(NOLOCK)
						   ON refi.object_id = sfk.referenced_object_id
					  INNER JOIN [set_database_name].sys.index_columns AS refic WITH(
					  NOLOCK)
						   ON refi.[object_id] = refic.[object_id]
					  INNER JOIN [set_database_name].sys.columns AS refc WITH(NOLOCK)
						   ON
							  refc.column_id = refic.column_id
							  AND refc.object_id = refic.object_id
					  INNER JOIN [set_database_name].sys.tables AS reft
						   ON refc.object_id = reft.object_id
					  LEFT JOIN [set_database_name].sys.schemas AS refs
						   ON refs.schema_id = reft.schema_id
				 WHERE t.object_id = @object_id),
			 cte
			 AS (SELECT 
						'ALTER TABLE '+QUOTENAME(schemaName)+'.'+QUOTENAME(
						tableName)+CASE is_not_trusted
										WHEN 0
											 THEN ' WITH CHECK '
										ELSE ' WITH NOCHECK '
								   END+' ADD CONSTRAINT '+QUOTENAME(fkName)+
								   ' FOREIGN KEY ('+fk_Cols+' ) '+CHAR(13)+
								   CHAR(10)+'REFERENCES '+QUOTENAME(
								   refSchemaName)+'.'+QUOTENAME(refTableName)+
								   ' ('+pk_Cols+')'+' ON UPDATE '+CASE update_referential_action
																	   WHEN 0
																			THEN
																			'NO ACTION '
																	   WHEN 1
																			THEN
																			'CASCADE '
																	   WHEN 2
																			THEN
																			'SET NULL '
																	   ELSE
																	   'SET DEFAULT '
																  END+
																  ' ON DELETE '+CASE delete_referential_action
																					 WHEN
																					 0
																						  THEN
																						  'NO ACTION '
																					 WHEN
																					 1
																						  THEN
																						  'CASCADE '
																					 WHEN
																					 2
																						  THEN
																						  'SET NULL '
																					 ELSE
																					 'SET DEFAULT '
																				END
																				+CASE is_not_for_replication
																					  WHEN
																					  1
																						   THEN
																						   ' NOT FOR REPLICATION '
																					  ELSE
																					  ''
																				 END
																				 +';'+CHAR(13)
																				 +CHAR(10)
																				 +'GO'+CHAR(13)
																				 +CHAR(10)
																				 +'ALTER TABLE '+QUOTENAME(schemaName)
																				 +'.'+QUOTENAME(tableName)
																				 +CASE is_disabled
																					   WHEN
																					   0
																							THEN
																							' CHECK '
																					   ELSE
																					   ' NOCHECK '
																				  END
																				  +'CONSTRAINT '+QUOTENAME(fkName)
																				  +';'+CHAR(13)
																				  +CHAR(10)
																				  +'GO'+CHAR(13)
																				  +CHAR(10)
																				  +CHAR(13)
																				  +CHAR(10)
																				  AS fk
				 FROM 
					  cteForeignKey)
			 SELECT 
					@fk = ISNULL(CHAR(13)+CHAR(10)+f.fk, '')
			 FROM 
				  cte AS f;


		SET @sql = CONCAT('CREATE TABLE ', @full_name+CHAR(13), '(', CHAR(13))
		+STUFF(
			(
			SELECT 
				   CONCAT(CHAR(13), '    , ', QUOTENAME(c.name), ' ')+CASE
																		   WHEN
																		   c.is_computed = 1
																				THEN
																				'AS '+computed_column.[definition]
																		   ELSE
																		   CASE
																				WHEN
																				c.system_type_id != c.user_type_id
																					 THEN
																					 QUOTENAME(UPPER(tp.name))
																				ELSE
																				IIF(UPPER(tp.name)
																				= 'numeric',
																				(QUOTENAME(UPPER(tp.name))
																				+'('+CAST(c.precision AS VARCHAR(10))
																				+','+CAST(c.scale AS VARCHAR(10))
																				+')'),
																				QUOTENAME(UPPER(tp.name)))
																		   END
																		   +CASE
																				 WHEN
																				 tp.name IN('varchar',
																				 'char',
																				 'varbinary',
																				 'binary')
																					  THEN
																					  '('+CASE
																							   WHEN
																							   c.max_length = -1
																									THEN
																									'MAX'
																							   ELSE
																							   CAST(c.max_length AS VARCHAR(5))
																						  END
																						  +')'
																				 WHEN
																				 tp.name IN('nvarchar',
																				 'nchar')
																					  THEN
																					  '('+CASE
																							   WHEN
																							   c.max_length = -1
																									THEN
																									'MAX'
																							   ELSE
																							   CAST(c.max_length/2 AS VARCHAR(5))
																						  END
																						  +')'
																				 WHEN
																				 tp.name IN('datetime2',
																				 'time2',
																				 'datetimeoffset')
																					  THEN
																					  '('+CAST(c.scale AS VARCHAR(5))
																					  +')'
																				 WHEN
																				 tp.name = 'decimal'
																					  THEN
																					  '('+CAST(c.[precision] AS VARCHAR(5))
																					  +','+CAST(c.scale AS VARCHAR(5))
																					  +')'
																				 ELSE
																				 ''
																			END
																			+
																		   CASE
																				WHEN
																				c.is_nullable = 1
																					 THEN
																					 ' NULL'
																				ELSE
																				' NOT NULL'
																		   END
																		   +CASE
																				 WHEN
																				 c.default_object_id != 0
																					  THEN
																					  ' CONSTRAINT ['+default_constraint.[name]+']'+
																					  ' DEFAULT '+default_constraint.[definition]
																				 ELSE
																				 ''
																			END
																			+CASE
																				  WHEN
																				  cc.[object_id] IS NOT NULL
																					   THEN
																					   ' CONSTRAINT ['+cc.name+'] CHECK '+cc.
																					   [definition]
																				  ELSE
																				  ''
																			 END
																			 +CASE
																				   WHEN
																				   c.is_identity = 1
																						THEN
																						' IDENTITY('+CAST(identity_column.seed_value AS
																						VARCHAR(10))
																						+','+CAST(identity_column.increment_value AS
																						VARCHAR(10))
																						+')'
																				   ELSE
																				   ''
																			  END
																	  END
			FROM 
				 [set_database_name].sys.columns AS c WITH(NOLOCK)
				 INNER JOIN [set_database_name].sys.types AS tp WITH(NOLOCK)
					  ON c.user_type_id = tp.user_type_id
				 LEFT JOIN [set_database_name].sys.check_constraints AS cc WITH(NOLOCK)
					  ON
						 c.[object_id] = cc.parent_object_id
						 AND cc.parent_column_id = c.column_id
				 LEFT JOIN [set_database_name].sys.computed_columns AS computed_column
				 WITH(NOLOCK)
					  ON
						 computed_column.column_id = c.column_id
						 AND c.object_id = computed_column.object_id
						 AND computed_column.name = c.name COLLATE
						 DATABASE_DEFAULT
				 LEFT JOIN [set_database_name].sys.default_constraints AS
				 default_constraint WITH(NOLOCK)
					  ON
						 default_constraint.parent_column_id = c.column_id
						 AND default_constraint.parent_object_id = c.object_id
				 LEFT JOIN [set_database_name].sys.identity_columns AS identity_column
				 WITH(NOLOCK)
					  ON
						 identity_column.object_id = c.object_id
						 AND identity_column.column_id = c.column_id
			WHERE c.[object_id] = @object_id
			ORDER BY 
					 c.column_id FOR
			XML PATH(''), TYPE
			).value( '.', 'NVARCHAR(MAX)' ), 1, 7, '      ')+ISNULL(
			(
			SELECT 
				   '  
			, CONSTRAINT ['+i.name+'] PRIMARY KEY '+CASE
														 WHEN i.index_id = 1
															  THEN 'CLUSTERED'
														 ELSE 'NONCLUSTERED'
													END+' ('+
				   (
				   SELECT 
						  STUFF(CAST(
						  (
						  SELECT 
								 ', ['+c.name+']'+CASE
													   WHEN ic.
													   is_descending_key = 1
															THEN ' DESC'
													   ELSE ''
												  END
						  FROM 
							   [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
							   INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
									ON
									   c.column_id = ic.column_id
									   AND c.object_id = ic.object_id
						  WHERE
								i.[object_id] = ic.[object_id]
								AND i.index_id = ic.index_id FOR
						  XML PATH(N''), TYPE
						  ) AS NVARCHAR(MAX)), 1, 2, '')
				   )+')'
			FROM 
				 [set_database_name].sys.indexes AS i WITH(NOLOCK)
			WHERE
				  i.[object_id] = @object_id
				  AND i.is_primary_key = 1
			), '')+CHAR(13)+ISNULL(
			(
			SELECT 
				   '  
			, CONSTRAINT ['+i.name+'] UNIQUE '+CASE
													WHEN i.index_id = 1
														 THEN 'CLUSTERED'
													ELSE 'NONCLUSTERED'
											   END+' ('+
				   (
				   SELECT 
						  STUFF(CAST(
						  (
						  SELECT 
								 ', ['+c.name+']'+CASE
													   WHEN ic.
													   is_descending_key = 1
															THEN ' DESC'
													   ELSE ''
												  END
						  FROM 
							   [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
							   INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
									ON
									   c.column_id = ic.column_id
									   AND c.object_id = ic.object_id
						  WHERE
								i.[object_id] = ic.[object_id]
								AND i.index_id = ic.index_id FOR
						  XML PATH(N''), TYPE
						  ) AS NVARCHAR(MAX)), 1, 2, '')
				   )+')'
			FROM 
				 [set_database_name].sys.indexes AS i WITH(NOLOCK)
			WHERE
				  i.[object_id] = @object_id
				  AND i.is_primary_key = 0
				  AND i.is_unique_constraint = 1
			), '')+CHAR(13)+');'+ISNULL(STUFF(
			(
			SELECT 
				   'CREATE '+IIF(i.is_unique = 1, 'UNIQUE ', '')+i.type_desc+
				   ' INDEX '+QUOTENAME(i.name)+SPACE(1)+'ON '+QUOTENAME(s.name)
				   +'.'+QUOTENAME(t.name)+SPACE(1)+'('+
				   (
				   SELECT 
						  STUFF(CAST(
						  (
						  SELECT 
								 ', ['+c.name+']'+CASE
													   WHEN ic.
													   is_descending_key = 1
															THEN ' DESC'
													   ELSE ' ASC'
												  END
						  FROM 
							   [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
							   INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
									ON
									   c.column_id = ic.column_id
									   AND c.object_id = ic.object_id
						  WHERE
								i.[object_id] = ic.[object_id]
								AND i.index_id = ic.index_id FOR
						  XML PATH(N''), TYPE
						  ) AS NVARCHAR(MAX)), 1, 2, '')
				   )+'); '
			FROM 
				 [set_database_name].sys.indexes AS i WITH(NOLOCK)
				 INNER JOIN [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
					  ON
						 i.[object_id] = ic.[object_id]
						 AND i.index_id = ic.index_id
				 INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
					  ON
						 c.column_id = ic.column_id
						 AND c.object_id = ic.object_id
				 INNER JOIN [set_database_name].sys.tables AS t
					  ON t.object_id = c.object_id
				 INNER JOIN [set_database_name].sys.schemas AS s
					  ON s.schema_id = t.schema_id
			WHERE
				  c.[object_id] = @object_id
				  AND i.is_primary_key = 0
				  AND i.is_unique_constraint = 0 FOR
				   XML PATH('')
			), 1, 0, ''), '');

		SET @sql = replace(replace(replace(@sql, ' ', '%#'), '#%', ''), '%#',
		' ')+ISNULL(@fk, '');
		UPDATE #TableScript
		SET 
			[table_script] = @sql
		WHERE 
			  row_id = @i;
		SET @i = @i+1;
		SET @fk = NULL;
	END;

	INSERT INTO [dbo].[t_object_history]
			   ([obh_db_name]
			   ,[obh_db_schema]
			   ,[obh_object_id]
			   ,[obh_object_name]
			   ,[obh_object_type]
			   ,[obh_object_definition]
			   ,[obh_object_created_datetime]
			   ,[obh_object_altered_datetime]
			   ,[obh_inserted_date])
		SELECT 
			   conf.dconf_db_name
			 , schemats.name
			 , all_object.object_id
			 , all_object.name
			 , all_object.type
			 , tb.[table_script]
			 , all_object.create_date
			 , all_object.modify_date
			 , GETDATE()
		FROM 
			 #TableScript tb
			 INNER JOIN [set_database_name].sys.all_objects AS all_object ON all_object.object_id = tb.[object_id] 
			 INNER JOIN [set_database_name].sys.schemas AS schemats		  ON all_object.schema_id = schemats.schema_id --AND tb.schema_name = schemats.name
			 INNER JOIN dbo.t_database_configuration AS conf
				  ON
					 conf.dconf_is_archived = 0
					 AND 'set_database_name' = conf.dconf_db_name COLLATE DATABASE_DEFAULT
			 LEFT JOIN dbo.t_object_history AS obj_hist
				  ON
					 obj_hist.obh_db_name = conf.dconf_db_name COLLATE
					 DATABASE_DEFAULT
					 AND obj_hist.obh_db_schema = schemats.name COLLATE
					 DATABASE_DEFAULT
					 AND obj_hist.obh_object_name = all_object.name COLLATE
					 DATABASE_DEFAULT
			 LEFT JOIN
			 (
			 SELECT 
					MAX(max_obj_hist.obh_object_altered_datetime) AS 'maximum'
				  , max_obj_hist.obh_db_name
				  , max_obj_hist.obh_db_schema
				  , max_obj_hist.obh_object_name
			 FROM 
				  dbo.t_object_history AS max_obj_hist
			 GROUP BY 
					  max_obj_hist.obh_db_name
					, max_obj_hist.obh_db_schema
					, max_obj_hist.obh_object_name
			 ) AS max_date
				  ON
					 max_date.obh_db_name = obj_hist.obh_db_name
					 AND max_date.obh_db_schema = obj_hist.obh_db_schema
					 AND max_date.obh_object_name = obj_hist.obh_object_name
					 AND max_date.maximum = obj_hist.obh_object_altered_datetime
		WHERE
			  obj_hist.obh_id IS NULL
			  OR max_date.maximum < all_object.modify_date;



END;
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[p_trigger_check_and_insert_all_changes_set_database_name]
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[t_object_history]
           ([obh_db_name]
           ,[obh_db_schema]
           ,[obh_object_id]
           ,[obh_object_name]
           ,[obh_object_type]
           ,[obh_object_definition]
           ,[obh_object_created_datetime]
           ,[obh_object_altered_datetime]
           ,[obh_inserted_date])
		  SELECT 
				 conf.dconf_db_name
				 , schemats.name
				 , all_object.object_id
				 , all_object.name
				 , all_object.type
				 , [sql_mod].[definition]
				 , all_object.create_date
				 , all_object.modify_date
				 , GETDATE()
			FROM 
				 [set_database_name].sys.all_objects AS all_object
				 INNER JOIN [set_database_name].sys.schemas AS schemats
					  ON all_object.schema_id = schemats.schema_id
				 INNER JOIN [set_database_name].sys.triggers AS trig
					  ON trig.object_id = all_object.object_id
				 INNER JOIN [set_database_name].sys.sql_modules sql_mod 
					 ON 
						[sql_mod].object_id = [all_object].object_id
				 INNER JOIN [set_database_name].sys.sql_modules AS sqlmod
					  ON
						 sqlmod.object_id = trig.object_id
				 INNER JOIN [set_database_name].sys.tables AS tab
					  ON tab.object_id = trig.parent_id
				 INNER JOIN [set_database_name].INFORMATION_SCHEMA.TABLES AS info_tab
					ON info_tab.TABLE_SCHEMA = schemats.name COLLATE DATABASE_DEFAULT
					AND info_tab.TABLE_NAME = tab.name COLLATE DATABASE_DEFAULT
				 INNER JOIN dbo.t_database_configuration AS conf
					  ON
						 conf.dconf_is_archived = 0
					 AND 'set_database_name' = conf.dconf_db_name COLLATE DATABASE_DEFAULT
				 LEFT JOIN dbo.t_object_history AS obj_hist
					  ON
						 obj_hist.obh_db_name = conf.dconf_db_name COLLATE
						 DATABASE_DEFAULT
						 AND obj_hist.obh_db_schema = schemats.name COLLATE
						 DATABASE_DEFAULT
						 AND obj_hist.obh_object_name = all_object.name COLLATE
						 DATABASE_DEFAULT
				 LEFT JOIN
				 (
				 SELECT 
						MAX(max_obj_hist.obh_object_altered_datetime) AS 'maximum'
					  , max_obj_hist.obh_db_name
					  , max_obj_hist.obh_db_schema
					  , max_obj_hist.obh_object_name
				 FROM 
					  dbo.t_object_history AS max_obj_hist
				 GROUP BY 
						  max_obj_hist.obh_db_name
						, max_obj_hist.obh_db_schema
						, max_obj_hist.obh_object_name
				 ) AS max_date
					  ON
						 max_date.obh_db_name = obj_hist.obh_db_name
						 AND max_date.obh_db_schema = obj_hist.obh_db_schema
						 AND max_date.obh_object_name = obj_hist.obh_object_name
						 AND max_date.maximum = obj_hist.obh_object_altered_datetime
			WHERE
				  obj_hist.obh_id IS NULL
				  OR max_date.maximum < all_object.modify_date;
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[p_view_check_and_insert_all_changes_set_database_name]
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[t_object_history]
           ([obh_db_name]
           ,[obh_db_schema]
           ,[obh_object_id]
           ,[obh_object_name]
           ,[obh_object_type]
           ,[obh_object_definition]
           ,[obh_object_created_datetime]
           ,[obh_object_altered_datetime]
           ,[obh_inserted_date])
		   SELECT 
				   conf.dconf_db_name
				 , schemats.name
				 , all_object.object_id
				 , all_object.name
				 , all_object.type
				 , [sql_mod].[definition]
				 , all_object.create_date
				 , all_object.modify_date
				 , GETDATE()
			FROM 
				 [set_database_name].sys.all_objects AS all_object
				 INNER JOIN [set_database_name].sys.schemas AS schemats
					  ON all_object.schema_id = schemats.schema_id
				 INNER JOIN [set_database_name].sys.views AS vie
					  ON vie.object_id = all_object.object_id
				 INNER JOIN [set_database_name].sys.sql_modules sql_mod 
				 ON 
					[sql_mod].object_id = [all_object].object_id
				 INNER JOIN [set_database_name].INFORMATION_SCHEMA.VIEWS AS infoschemaview
					  ON
						 infoschemaview.TABLE_SCHEMA = schemats.name
						 AND all_object.name = infoschemaview.TABLE_NAME
				 INNER JOIN dbo.t_database_configuration AS conf
					  ON
						 conf.dconf_is_archived = 0
						AND 'set_database_name' = conf.dconf_db_name COLLATE DATABASE_DEFAULT
				 LEFT JOIN dbo.t_object_history AS obj_hist
					  ON
						 obj_hist.obh_db_name = conf.dconf_db_name COLLATE
						 DATABASE_DEFAULT
						 AND obj_hist.obh_db_schema = schemats.name COLLATE
						 DATABASE_DEFAULT
						 AND obj_hist.obh_object_name = all_object.name COLLATE
						 DATABASE_DEFAULT
				 LEFT JOIN
				 (
				 SELECT 
						MAX(max_obj_hist.obh_object_altered_datetime) AS 'maximum'
					  , max_obj_hist.obh_db_name
					  , max_obj_hist.obh_db_schema
					  , max_obj_hist.obh_object_name
				 FROM 
					  dbo.t_object_history AS max_obj_hist
				 GROUP BY 
						  max_obj_hist.obh_db_name
						, max_obj_hist.obh_db_schema
						, max_obj_hist.obh_object_name
				 ) AS max_date
					  ON
						 max_date.obh_db_name = obj_hist.obh_db_name
						 AND max_date.obh_db_schema = obj_hist.obh_db_schema
						 AND max_date.obh_object_name = obj_hist.obh_object_name
						 AND max_date.maximum = obj_hist.obh_object_altered_datetime
			WHERE
				  obj_hist.obh_id IS NULL
				  OR max_date.maximum < all_object.modify_date;
END  

GO
