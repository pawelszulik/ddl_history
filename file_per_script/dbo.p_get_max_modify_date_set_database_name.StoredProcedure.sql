USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROCEDURE [dbo].[p_get_max_modify_date_set_database_name]
@type char(2), @max_datetime DATETIME OUTPUT
AS
BEGIN
	SET NOCOUNT ON;


	SELECT 
		   @max_datetime = MAX(all_obj.modify_date)
	FROM 
		 [set_database_name].sys.all_objects AS all_obj
	WHERE
		all_obj.type = @type;

END;
GO
