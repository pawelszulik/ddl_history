USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[p_trigger_check_and_insert_all_changes_set_database_name]
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[t_object_history]
           ([obh_db_name]
           ,[obh_db_schema]
           ,[obh_object_id]
           ,[obh_object_name]
           ,[obh_object_type]
           ,[obh_object_definition]
           ,[obh_object_created_datetime]
           ,[obh_object_altered_datetime]
           ,[obh_inserted_date])
		  SELECT 
				 conf.dconf_db_name
				 , schemats.name
				 , all_object.object_id
				 , all_object.name
				 , all_object.type
				 , [sql_mod].[definition]
				 , all_object.create_date
				 , all_object.modify_date
				 , GETDATE()
			FROM 
				 [set_database_name].sys.all_objects AS all_object
				 INNER JOIN [set_database_name].sys.schemas AS schemats
					  ON all_object.schema_id = schemats.schema_id
				 INNER JOIN [set_database_name].sys.triggers AS trig
					  ON trig.object_id = all_object.object_id
				 INNER JOIN [set_database_name].sys.sql_modules sql_mod 
					 ON 
						[sql_mod].object_id = [all_object].object_id
				 INNER JOIN [set_database_name].sys.sql_modules AS sqlmod
					  ON
						 sqlmod.object_id = trig.object_id
				 INNER JOIN [set_database_name].sys.tables AS tab
					  ON tab.object_id = trig.parent_id
				 INNER JOIN [set_database_name].INFORMATION_SCHEMA.TABLES AS info_tab
					ON info_tab.TABLE_SCHEMA = schemats.name COLLATE DATABASE_DEFAULT
					AND info_tab.TABLE_NAME = tab.name COLLATE DATABASE_DEFAULT
				 INNER JOIN dbo.t_database_configuration AS conf
					  ON
						 conf.dconf_is_archived = 0
					 AND 'set_database_name' = conf.dconf_db_name COLLATE DATABASE_DEFAULT
				 LEFT JOIN dbo.t_object_history AS obj_hist
					  ON
						 obj_hist.obh_db_name = conf.dconf_db_name COLLATE
						 DATABASE_DEFAULT
						 AND obj_hist.obh_db_schema = schemats.name COLLATE
						 DATABASE_DEFAULT
						 AND obj_hist.obh_object_name = all_object.name COLLATE
						 DATABASE_DEFAULT
				 LEFT JOIN
				 (
				 SELECT 
						MAX(max_obj_hist.obh_object_altered_datetime) AS 'maximum'
					  , max_obj_hist.obh_db_name
					  , max_obj_hist.obh_db_schema
					  , max_obj_hist.obh_object_name
				 FROM 
					  dbo.t_object_history AS max_obj_hist
				 GROUP BY 
						  max_obj_hist.obh_db_name
						, max_obj_hist.obh_db_schema
						, max_obj_hist.obh_object_name
				 ) AS max_date
					  ON
						 max_date.obh_db_name = obj_hist.obh_db_name
						 AND max_date.obh_db_schema = obj_hist.obh_db_schema
						 AND max_date.obh_object_name = obj_hist.obh_object_name
						 AND max_date.maximum = obj_hist.obh_object_altered_datetime
			WHERE
				  obj_hist.obh_id IS NULL
				  OR max_date.maximum < all_object.modify_date;
END
GO
