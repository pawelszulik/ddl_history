USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_check_and_insert_all_changes]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @database [varchar](128);
	DECLARE @is_logging_table  bit;
	DECLARE @last_change_table  DATETIME
	DECLARE @is_logging_trigger  bit;
	DECLARE @last_change_trigger  DATETIME
	DECLARE @is_logging_programmability  bit;
	DECLARE @last_change_programmability  DATETIME
	DECLARE @is_logging_view  bit;
	DECLARE @last_change_view  DATETIME
	DECLARE @rows INT;
	DECLARE @i INT = 1;
	DECLARE @current_date DATETIME = GETDATE();
	DECLARE @last_change_date DATETIME;
	DECLARE @proc_to_create NVARCHAR(MAX);
	DECLARE @proc NVARCHAR(MAX);
	DECLARE @sql NVARCHAR(MAX);
	DECLARE @type CHAR(2);
	DECLARE @local_max_date DATETIME;

	-- Pobranie wszystkich baz w konfiguracji, które nie są archiwalne
	-- Odfiltrowanie baz które są offline
	IF OBJECT_ID('tempdb..#tmp_database') IS NOT NULL DROP TABLE #tmp_database
	CREATE TABLE #tmp_database
	(
		[ordinal_number] INT NOT NULL,
		[database] [varchar](128) NOT NULL,
		[is_logging_table]  bit NOT NULL,
		[last_change_table]  DATETIME NULL,
		[is_logging_trigger]  bit NOT NULL,
		[last_change_trigger]  DATETIME NULL,
		[is_logging_programmability]  bit NOT NULL,
		[last_change_programmability]  DATETIME NULL,
		[is_logging_view]  bit NOT NULL,
		[last_change_view]  DATETIME NULL
	);
	INSERT INTO 
		#tmp_database 
		(
			[ordinal_number]
			, [database]
			, [is_logging_table]
			, [last_change_table]
			, [is_logging_trigger]
			, [last_change_trigger]
			, [is_logging_programmability]
			, [last_change_programmability]
			, [is_logging_view]
			, [last_change_view]
		)
	SELECT 
		ROW_NUMBER() OVER (order by d.database_id asc)
		, [dconf_db_name]
		  ,[dconf_is_logging_table]
		  ,[dconf_last_change_table]
		  ,[dconf_is_logging_trigger]
		  ,[dconf_last_change_trigger]
		  ,[dconf_is_logging_programmability]
		  ,[dconf_last_change_programmability]
		  ,[dconf_is_logging_view]
		  ,[dconf_last_change_view]
	FROM 
		[dbo].[t_database_configuration]
		INNER JOIN sys.databases d ON d.[name] = [dconf_db_name] AND d.[state] = 0 AND d.database_id > 4
	WHERE
		dconf_is_archived = 0;

	SELECT 
		   @rows = COUNT(1)
	FROM 
		 #tmp_database;
		 
	-- Dla każdej z baz
	WHILE @i <= @rows
		BEGIN
			SELECT 
				 @database = [database]
				, @is_logging_table = [is_logging_table]
				, @last_change_table = [last_change_table]
				, @is_logging_trigger = [is_logging_trigger]
				, @last_change_trigger = [last_change_trigger]
				, @is_logging_programmability = [is_logging_programmability]
				, @last_change_programmability = [last_change_programmability]
				, @is_logging_view = [is_logging_view]
				, @last_change_view = [last_change_view]
			FROM 
				#tmp_database
			WHERE
				[ordinal_number] = @i;
						
			-- Jezeli nie istnie procedura do wyciagania max daty modyfikacji
			IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_get_max_modify_date_' + @database))
				BEGIN				
					SELECT 
						@proc_to_create = REPLACE([ROUTINE_DEFINITION], 'set_database_name', @database)
					FROM 
						INFORMATION_SCHEMA.routines
					WHERE 
						routine_name = 'p_get_max_modify_date_set_database_name';
					-- Tworz ja
					EXEC (@proc_to_create);
				END

			-- Tabele
			IF (@is_logging_table = 1)
			BEGIN	
				-- Typ tabeli
				SET @type = 'U';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				
				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_table < @last_change_date
				BEGIN
					-- Jezeli nie istnieje procedura
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_table_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_table_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure do tworzenia skryptow
					SET @sql = CONCAT('p_table_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;
					
					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_table] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END						

			END	

			-- programmability			
			IF (@is_logging_programmability = 1)
			BEGIN	
				-- Typ
				SET @type = 'P';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				SET @local_max_date = @last_change_date;
				
				-- Typ
				SET @type = 'FN';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT

				IF @local_max_date < @last_change_date
					BEGIN
						SET @local_max_date = @last_change_date;
					END
				
				-- Typ
				SET @type = 'TF';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT

				IF @local_max_date < @last_change_date
					BEGIN
						SET @local_max_date = @last_change_date;
					END

				SET @last_change_date = @local_max_date;

				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_programmability < @last_change_date
				BEGIN
					-- Jezeli nie istnieje procedura
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_programmability_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_programmability_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure
					SET @sql = CONCAT('p_programmability_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;

					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_programmability] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END						
			END

			
			-- widok			
			IF (@is_logging_view = 1)
			BEGIN	
				-- Typ
				SET @type = 'V';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				
				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_view < @last_change_date
				BEGIN
					-- Jezeli nie istnieje tabela do tworzenia skryptow tabel
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_view_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_view_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure
					SET @sql = CONCAT('p_view_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;
					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_view] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END							
			END

			
			-- trigger			
			IF (@is_logging_trigger = 1)
			BEGIN	
				-- Typ
				SET @type = 'TR';
				
				-- Pobierz ostatnia date modyfikacji
				SET @sql = CONCAT(N'p_get_max_modify_date_', @database,' @type, @last_change_date OUTPUT');
				EXECUTE sp_executesql @sql, N'@type CHAR(2), @last_change_date DATETIME OUTPUT'
										, @type=@type, @last_change_date=@last_change_date OUTPUT
				
				-- Jezeli nastapila zmiana od ostatniego zapisu 
				IF @last_change_trigger < @last_change_date
				BEGIN
					-- Jezeli nie istnieje tabela do tworzenia skryptow tabel
					IF NOT EXISTS (SELECT 1	FROM INFORMATION_SCHEMA.routines WHERE routine_name = ('p_trigger_check_and_insert_all_changes_' + @database))
						BEGIN	
							
							SET @proc = REPLACE(
							OBJECT_DEFINITION (OBJECT_ID(N'p_trigger_check_and_insert_all_changes_set_database_name')), N'set_database_name', @database);
							-- Stworz ja
							EXECUTE sp_executesql @proc;													
						END
						
					-- Wykonaj procedure
					SET @sql = CONCAT('p_trigger_check_and_insert_all_changes_', @database);
					EXECUTE sp_executesql @sql;
					-- Aktualizacja daty
					UPDATE
						dc
					SET
						dc.[dconf_last_change_trigger] = @last_change_date
					FROM 
						[dbo].[t_database_configuration] dc
					WHERE
						dc.[dconf_db_name] = @database;
				END
			END

			SET @i = @i + 1;
		END;
	
	UPDATE
		dc
	SET
		dc.[dconf_last_check_date] = @current_date
	FROM 
		[dbo].[t_database_configuration] dc
		INNER JOIN sys.databases d ON d.[name] = dc.[dconf_db_name] AND d.[state] = 0 AND d.database_id > 4
	WHERE
		dc.dconf_is_archived = 0

END


GO
