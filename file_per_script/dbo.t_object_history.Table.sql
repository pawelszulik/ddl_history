USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_object_history](
	[obh_id] [int] IDENTITY(1,1) NOT NULL,
	[obh_db_name] [varchar](128) NOT NULL,
	[obh_db_schema] [varchar](128) NOT NULL,
	[obh_object_id] [int] NOT NULL,
	[obh_object_name] [varchar](128) NOT NULL,
	[obh_object_type] [varchar](128) NOT NULL,
	[obh_object_definition] [nvarchar](max) NOT NULL,
	[obh_object_created_datetime] [datetime] NOT NULL,
	[obh_object_altered_datetime] [datetime] NOT NULL,
	[obh_inserted_date] [datetime] NOT NULL,
 CONSTRAINT [PK_obh_id] PRIMARY KEY CLUSTERED 
(
	[obh_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_obh] ON [dbo].[t_object_history]
(
	[obh_db_name] ASC,
	[obh_db_schema] ASC,
	[obh_object_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t_object_history] ADD  DEFAULT (getdate()) FOR [obh_inserted_date]
GO
