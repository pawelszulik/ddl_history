USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE   PROCEDURE [dbo].[p_add_new_database_to_configuration] 
				 @database_name VARCHAR(128)
			   , @is_logging_table bit = 1
			   , @is_logging_trigger bit = 1
			   , @is_logging_programmability bit = 1
			   , @is_logging_view bit = 1
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE 
		   @is_exists BIT;
	SET @is_exists = ISNULL(
		(
		SELECT 
			   1
		FROM 
			 [dbo].[t_database_configuration]
		WHERE [dconf_db_name] = @database_name
		), 0);

	IF @is_exists = 1
		BEGIN
			RAISERROR('Taka baza już istnieje!', 16, 1);
			RETURN;
		END;

	INSERT INTO [dbo].[t_database_configuration]
		   (
		   [dconf_db_name]
		   ,[dconf_is_archived]
           ,[dconf_is_logging_table]
           ,[dconf_is_logging_trigger]
           ,[dconf_is_logging_programmability]
           ,[dconf_is_logging_view]
		   )
	VALUES
		   ( 
		   @database_name
		   ,0
		   , @is_logging_table
		   , @is_logging_trigger
		   , @is_logging_programmability
		   , @is_logging_view
		   );
END;
GO
