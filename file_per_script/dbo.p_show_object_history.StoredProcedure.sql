USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[p_show_object_history] 
				 @database_name VARCHAR(128)
				 , @obect_type char(2) = null
				 , @object_name VARCHAR(128) = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[obh_db_name]						AS 'Database name'
		,[obh_db_schema]					AS 'Schema name'
		,[obh_object_id]					AS 'Object Id'
		,[obh_object_name]					AS 'Object name'
		,[obh_object_type]					AS 'Object type'
		,[obh_object_definition]			AS 'Object definition'
		,[obh_object_created_datetime]		AS 'Object create date'
		,[obh_object_altered_datetime]		AS 'Object alter date'
		,[obh_inserted_date]				AS 'Insert date'
	FROM 
		[dbo].[t_object_history]
	WHERE
		[obh_db_name] = @database_name
		AND (@obect_type IS NULL OR [obh_object_type] = @obect_type)
		AND (@object_name IS NULL OR [obh_object_name] = @object_name)
	ORDER BY
		[obh_db_name] ASC, [obh_db_schema] ASC, [obh_object_type] ASC, [obh_object_name] ASC, [obh_object_altered_datetime] DESC;

END;
GO
