USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_database_configuration](
	[dconf_db_name] [varchar](128) NOT NULL,
	[dconf_is_archived] [bit] NOT NULL,
	[dconf_last_check_date] [datetime] NOT NULL,
	[dconf_is_logging_table] [bit] NOT NULL,
	[dconf_last_change_table] [datetime] NOT NULL,
	[dconf_is_logging_trigger] [bit] NOT NULL,
	[dconf_last_change_trigger] [datetime] NOT NULL,
	[dconf_is_logging_programmability] [bit] NOT NULL,
	[dconf_last_change_programmability] [datetime] NOT NULL,
	[dconf_is_logging_view] [bit] NOT NULL,
	[dconf_last_change_view] [datetime] NOT NULL,
 CONSTRAINT [PK_t_database_configuration] PRIMARY KEY CLUSTERED 
(
	[dconf_db_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_configuration_conf_last_check_date]  DEFAULT (sysdatetimeoffset()) FOR [dconf_last_check_date]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_table]  DEFAULT ((1)) FOR [dconf_is_logging_table]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_table]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_trigger]  DEFAULT ((1)) FOR [dconf_is_logging_trigger]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_trigger]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_stored_procedure]  DEFAULT ((1)) FOR [dconf_is_logging_programmability]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_programmability]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_is_logging_view]  DEFAULT ((1)) FOR [dconf_is_logging_view]
GO
ALTER TABLE [dbo].[t_database_configuration] ADD  CONSTRAINT [DF_t_database_configuration_dconf_last_change_view]  DEFAULT (CONVERT([datetime],(-53690))) FOR [dconf_last_change_view]
GO
