USE [DDL_History]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_table_check_and_insert_all_changes_set_database_name]
AS
BEGIN
SET NOCOUNT ON;

	DECLARE 
		   @schema_name SYSNAME
		 , @table_name  SYSNAME
		 , @full_name   SYSNAME
		 , @object_id   INT
		 , @sql         NVARCHAR(MAX)
		 , @fk          NVARCHAR(MAX);
	IF OBJECT_ID('tempdb..#TableScript') IS NOT NULL
	BEGIN
		DROP TABLE #TableScript;
	END;

	CREATE TABLE #TableScript
				 (
				 [row_id]       [INT] NOT NULL
			   , [table_name]   [VARCHAR](255) NOT NULL
			   , [schema_name]  [VARCHAR](255) NOT NULL
			   , [full_name]    [VARCHAR](255) NOT NULL
			   , [object_id]    [INT] NOT NULL
			   , [table_script] NVARCHAR(MAX) NULL
				 );

	INSERT INTO #TableScript
		   (
		   row_id
		 , [table_name]
		 , [schema_name]
		 , [full_name]
		 , object_id
		   )
		   SELECT 
				  ROW_NUMBER() OVER(
				  ORDER BY 
						   tab.object_id)
				, QUOTENAME(tab.name)
				, QUOTENAME(s.name)
				, CONCAT(QUOTENAME(s.name), '.', QUOTENAME(tab.name))
				, tab.object_id
		   FROM 
				[set_database_name].sys.tables AS tab
				INNER JOIN [set_database_name].sys.schemas AS s
					 ON tab.schema_id = s.schema_id;

	DECLARE 
		   @rows INT;
	DECLARE 
		   @i INT = 1;

	SELECT 
		   @rows = COUNT(1)
	FROM 
		 #TableScript;

	WHILE @i <= @rows
	BEGIN
		SELECT 
			   @full_name = [full_name]
			 , @object_id = [object_id]
			 , @schema_name = [schema_name]
			 , @table_name = [table_name]
		FROM 
			 #TableScript
		WHERE row_id = @i;
		WITH cteForeignKey
			 AS (SELECT 
						s.name AS        schemaName
					  , t.[name] AS      tableName
					  , sfk.[name] AS    fkName
					  , refs.name AS     refSchemaName
					  , reft.name AS     refTablename
					  , sfk.object_id AS fkObjectID
					  , sfk.is_disabled
					  , sfk.is_not_for_replication
					  , sfk.is_not_trusted
					  , sfk.delete_referential_action
					  , sfk.update_referential_action
					  , pk_cols = STUFF(
						(
						SELECT 
							   ','+refc.name
						FROM 
							 [set_database_name].sys.foreign_key_columns AS fkc
						WHERE fkc.constraint_object_id = sfk.object_id FOR
						XML PATH('')
						), 1, 1, '')
					  , fk_cols = STUFF(
						(
						SELECT 
							   ','+fparc.name
						FROM 
							 [set_database_name].sys.foreign_key_columns AS fkc
							 INNER JOIN [set_database_name].sys.columns AS fparc WITH(
							 NOLOCK)
								  ON
									 fparc.column_id = fkc.parent_column_id
									 AND fparc.object_id = fkc.
									 parent_object_id
						WHERE fkc.constraint_object_id = sfk.object_id FOR
						XML PATH('')
						), 1, 1, '')
				 FROM 
					  [set_database_name].sys.foreign_keys AS sfk
					  INNER JOIN [set_database_name].sys.tables AS t
						   ON t.object_id = sfk.parent_object_id
					  INNER JOIN [set_database_name].sys.schemas AS s
						   ON s.schema_id = t.schema_id
					  INNER JOIN [set_database_name].sys.indexes AS refi WITH(NOLOCK)
						   ON refi.object_id = sfk.referenced_object_id
					  INNER JOIN [set_database_name].sys.index_columns AS refic WITH(
					  NOLOCK)
						   ON refi.[object_id] = refic.[object_id]
					  INNER JOIN [set_database_name].sys.columns AS refc WITH(NOLOCK)
						   ON
							  refc.column_id = refic.column_id
							  AND refc.object_id = refic.object_id
					  INNER JOIN [set_database_name].sys.tables AS reft
						   ON refc.object_id = reft.object_id
					  LEFT JOIN [set_database_name].sys.schemas AS refs
						   ON refs.schema_id = reft.schema_id
				 WHERE t.object_id = @object_id),
			 cte
			 AS (SELECT 
						'ALTER TABLE '+QUOTENAME(schemaName)+'.'+QUOTENAME(
						tableName)+CASE is_not_trusted
										WHEN 0
											 THEN ' WITH CHECK '
										ELSE ' WITH NOCHECK '
								   END+' ADD CONSTRAINT '+QUOTENAME(fkName)+
								   ' FOREIGN KEY ('+fk_Cols+' ) '+CHAR(13)+
								   CHAR(10)+'REFERENCES '+QUOTENAME(
								   refSchemaName)+'.'+QUOTENAME(refTableName)+
								   ' ('+pk_Cols+')'+' ON UPDATE '+CASE update_referential_action
																	   WHEN 0
																			THEN
																			'NO ACTION '
																	   WHEN 1
																			THEN
																			'CASCADE '
																	   WHEN 2
																			THEN
																			'SET NULL '
																	   ELSE
																	   'SET DEFAULT '
																  END+
																  ' ON DELETE '+CASE delete_referential_action
																					 WHEN
																					 0
																						  THEN
																						  'NO ACTION '
																					 WHEN
																					 1
																						  THEN
																						  'CASCADE '
																					 WHEN
																					 2
																						  THEN
																						  'SET NULL '
																					 ELSE
																					 'SET DEFAULT '
																				END
																				+CASE is_not_for_replication
																					  WHEN
																					  1
																						   THEN
																						   ' NOT FOR REPLICATION '
																					  ELSE
																					  ''
																				 END
																				 +';'+CHAR(13)
																				 +CHAR(10)
																				 +'GO'+CHAR(13)
																				 +CHAR(10)
																				 +'ALTER TABLE '+QUOTENAME(schemaName)
																				 +'.'+QUOTENAME(tableName)
																				 +CASE is_disabled
																					   WHEN
																					   0
																							THEN
																							' CHECK '
																					   ELSE
																					   ' NOCHECK '
																				  END
																				  +'CONSTRAINT '+QUOTENAME(fkName)
																				  +';'+CHAR(13)
																				  +CHAR(10)
																				  +'GO'+CHAR(13)
																				  +CHAR(10)
																				  +CHAR(13)
																				  +CHAR(10)
																				  AS fk
				 FROM 
					  cteForeignKey)
			 SELECT 
					@fk = ISNULL(CHAR(13)+CHAR(10)+f.fk, '')
			 FROM 
				  cte AS f;


		SET @sql = CONCAT('CREATE TABLE ', @full_name+CHAR(13), '(', CHAR(13))
		+STUFF(
			(
			SELECT 
				   CONCAT(CHAR(13), '    , ', QUOTENAME(c.name), ' ')+CASE
																		   WHEN
																		   c.is_computed = 1
																				THEN
																				'AS '+computed_column.[definition]
																		   ELSE
																		   CASE
																				WHEN
																				c.system_type_id != c.user_type_id
																					 THEN
																					 QUOTENAME(UPPER(tp.name))
																				ELSE
																				IIF(UPPER(tp.name)
																				= 'numeric',
																				(QUOTENAME(UPPER(tp.name))
																				+'('+CAST(c.precision AS VARCHAR(10))
																				+','+CAST(c.scale AS VARCHAR(10))
																				+')'),
																				QUOTENAME(UPPER(tp.name)))
																		   END
																		   +CASE
																				 WHEN
																				 tp.name IN('varchar',
																				 'char',
																				 'varbinary',
																				 'binary')
																					  THEN
																					  '('+CASE
																							   WHEN
																							   c.max_length = -1
																									THEN
																									'MAX'
																							   ELSE
																							   CAST(c.max_length AS VARCHAR(5))
																						  END
																						  +')'
																				 WHEN
																				 tp.name IN('nvarchar',
																				 'nchar')
																					  THEN
																					  '('+CASE
																							   WHEN
																							   c.max_length = -1
																									THEN
																									'MAX'
																							   ELSE
																							   CAST(c.max_length/2 AS VARCHAR(5))
																						  END
																						  +')'
																				 WHEN
																				 tp.name IN('datetime2',
																				 'time2',
																				 'datetimeoffset')
																					  THEN
																					  '('+CAST(c.scale AS VARCHAR(5))
																					  +')'
																				 WHEN
																				 tp.name = 'decimal'
																					  THEN
																					  '('+CAST(c.[precision] AS VARCHAR(5))
																					  +','+CAST(c.scale AS VARCHAR(5))
																					  +')'
																				 ELSE
																				 ''
																			END
																			+
																		   CASE
																				WHEN
																				c.is_nullable = 1
																					 THEN
																					 ' NULL'
																				ELSE
																				' NOT NULL'
																		   END
																		   +CASE
																				 WHEN
																				 c.default_object_id != 0
																					  THEN
																					  ' CONSTRAINT ['+default_constraint.[name]+']'+
																					  ' DEFAULT '+default_constraint.[definition]
																				 ELSE
																				 ''
																			END
																			+CASE
																				  WHEN
																				  cc.[object_id] IS NOT NULL
																					   THEN
																					   ' CONSTRAINT ['+cc.name+'] CHECK '+cc.
																					   [definition]
																				  ELSE
																				  ''
																			 END
																			 +CASE
																				   WHEN
																				   c.is_identity = 1
																						THEN
																						' IDENTITY('+CAST(identity_column.seed_value AS
																						VARCHAR(10))
																						+','+CAST(identity_column.increment_value AS
																						VARCHAR(10))
																						+')'
																				   ELSE
																				   ''
																			  END
																	  END
			FROM 
				 [set_database_name].sys.columns AS c WITH(NOLOCK)
				 INNER JOIN [set_database_name].sys.types AS tp WITH(NOLOCK)
					  ON c.user_type_id = tp.user_type_id
				 LEFT JOIN [set_database_name].sys.check_constraints AS cc WITH(NOLOCK)
					  ON
						 c.[object_id] = cc.parent_object_id
						 AND cc.parent_column_id = c.column_id
				 LEFT JOIN [set_database_name].sys.computed_columns AS computed_column
				 WITH(NOLOCK)
					  ON
						 computed_column.column_id = c.column_id
						 AND c.object_id = computed_column.object_id
						 AND computed_column.name = c.name COLLATE
						 DATABASE_DEFAULT
				 LEFT JOIN [set_database_name].sys.default_constraints AS
				 default_constraint WITH(NOLOCK)
					  ON
						 default_constraint.parent_column_id = c.column_id
						 AND default_constraint.parent_object_id = c.object_id
				 LEFT JOIN [set_database_name].sys.identity_columns AS identity_column
				 WITH(NOLOCK)
					  ON
						 identity_column.object_id = c.object_id
						 AND identity_column.column_id = c.column_id
			WHERE c.[object_id] = @object_id
			ORDER BY 
					 c.column_id FOR
			XML PATH(''), TYPE
			).value( '.', 'NVARCHAR(MAX)' ), 1, 7, '      ')+ISNULL(
			(
			SELECT 
				   '  
			, CONSTRAINT ['+i.name+'] PRIMARY KEY '+CASE
														 WHEN i.index_id = 1
															  THEN 'CLUSTERED'
														 ELSE 'NONCLUSTERED'
													END+' ('+
				   (
				   SELECT 
						  STUFF(CAST(
						  (
						  SELECT 
								 ', ['+c.name+']'+CASE
													   WHEN ic.
													   is_descending_key = 1
															THEN ' DESC'
													   ELSE ''
												  END
						  FROM 
							   [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
							   INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
									ON
									   c.column_id = ic.column_id
									   AND c.object_id = ic.object_id
						  WHERE
								i.[object_id] = ic.[object_id]
								AND i.index_id = ic.index_id FOR
						  XML PATH(N''), TYPE
						  ) AS NVARCHAR(MAX)), 1, 2, '')
				   )+')'
			FROM 
				 [set_database_name].sys.indexes AS i WITH(NOLOCK)
			WHERE
				  i.[object_id] = @object_id
				  AND i.is_primary_key = 1
			), '')+CHAR(13)+ISNULL(
			(
			SELECT 
				   '  
			, CONSTRAINT ['+i.name+'] UNIQUE '+CASE
													WHEN i.index_id = 1
														 THEN 'CLUSTERED'
													ELSE 'NONCLUSTERED'
											   END+' ('+
				   (
				   SELECT 
						  STUFF(CAST(
						  (
						  SELECT 
								 ', ['+c.name+']'+CASE
													   WHEN ic.
													   is_descending_key = 1
															THEN ' DESC'
													   ELSE ''
												  END
						  FROM 
							   [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
							   INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
									ON
									   c.column_id = ic.column_id
									   AND c.object_id = ic.object_id
						  WHERE
								i.[object_id] = ic.[object_id]
								AND i.index_id = ic.index_id FOR
						  XML PATH(N''), TYPE
						  ) AS NVARCHAR(MAX)), 1, 2, '')
				   )+')'
			FROM 
				 [set_database_name].sys.indexes AS i WITH(NOLOCK)
			WHERE
				  i.[object_id] = @object_id
				  AND i.is_primary_key = 0
				  AND i.is_unique_constraint = 1
			), '')+CHAR(13)+');'+ISNULL(STUFF(
			(
			SELECT 
				   'CREATE '+IIF(i.is_unique = 1, 'UNIQUE ', '')+i.type_desc+
				   ' INDEX '+QUOTENAME(i.name)+SPACE(1)+'ON '+QUOTENAME(s.name)
				   +'.'+QUOTENAME(t.name)+SPACE(1)+'('+
				   (
				   SELECT 
						  STUFF(CAST(
						  (
						  SELECT 
								 ', ['+c.name+']'+CASE
													   WHEN ic.
													   is_descending_key = 1
															THEN ' DESC'
													   ELSE ' ASC'
												  END
						  FROM 
							   [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
							   INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
									ON
									   c.column_id = ic.column_id
									   AND c.object_id = ic.object_id
						  WHERE
								i.[object_id] = ic.[object_id]
								AND i.index_id = ic.index_id FOR
						  XML PATH(N''), TYPE
						  ) AS NVARCHAR(MAX)), 1, 2, '')
				   )+'); '
			FROM 
				 [set_database_name].sys.indexes AS i WITH(NOLOCK)
				 INNER JOIN [set_database_name].sys.index_columns AS ic WITH(NOLOCK)
					  ON
						 i.[object_id] = ic.[object_id]
						 AND i.index_id = ic.index_id
				 INNER JOIN [set_database_name].sys.columns AS c WITH(NOLOCK)
					  ON
						 c.column_id = ic.column_id
						 AND c.object_id = ic.object_id
				 INNER JOIN [set_database_name].sys.tables AS t
					  ON t.object_id = c.object_id
				 INNER JOIN [set_database_name].sys.schemas AS s
					  ON s.schema_id = t.schema_id
			WHERE
				  c.[object_id] = @object_id
				  AND i.is_primary_key = 0
				  AND i.is_unique_constraint = 0 FOR
				   XML PATH('')
			), 1, 0, ''), '');

		SET @sql = replace(replace(replace(@sql, ' ', '%#'), '#%', ''), '%#',
		' ')+ISNULL(@fk, '');
		UPDATE #TableScript
		SET 
			[table_script] = @sql
		WHERE 
			  row_id = @i;
		SET @i = @i+1;
		SET @fk = NULL;
	END;

	INSERT INTO [dbo].[t_object_history]
			   ([obh_db_name]
			   ,[obh_db_schema]
			   ,[obh_object_id]
			   ,[obh_object_name]
			   ,[obh_object_type]
			   ,[obh_object_definition]
			   ,[obh_object_created_datetime]
			   ,[obh_object_altered_datetime]
			   ,[obh_inserted_date])
		SELECT 
			   conf.dconf_db_name
			 , schemats.name
			 , all_object.object_id
			 , all_object.name
			 , all_object.type
			 , tb.[table_script]
			 , all_object.create_date
			 , all_object.modify_date
			 , GETDATE()
		FROM 
			 #TableScript tb
			 INNER JOIN [set_database_name].sys.all_objects AS all_object ON all_object.object_id = tb.[object_id] 
			 INNER JOIN [set_database_name].sys.schemas AS schemats		  ON all_object.schema_id = schemats.schema_id --AND tb.schema_name = schemats.name
			 INNER JOIN dbo.t_database_configuration AS conf
				  ON
					 conf.dconf_is_archived = 0
					 AND 'set_database_name' = conf.dconf_db_name COLLATE DATABASE_DEFAULT
			 LEFT JOIN dbo.t_object_history AS obj_hist
				  ON
					 obj_hist.obh_db_name = conf.dconf_db_name COLLATE
					 DATABASE_DEFAULT
					 AND obj_hist.obh_db_schema = schemats.name COLLATE
					 DATABASE_DEFAULT
					 AND obj_hist.obh_object_name = all_object.name COLLATE
					 DATABASE_DEFAULT
			 LEFT JOIN
			 (
			 SELECT 
					MAX(max_obj_hist.obh_object_altered_datetime) AS 'maximum'
				  , max_obj_hist.obh_db_name
				  , max_obj_hist.obh_db_schema
				  , max_obj_hist.obh_object_name
			 FROM 
				  dbo.t_object_history AS max_obj_hist
			 GROUP BY 
					  max_obj_hist.obh_db_name
					, max_obj_hist.obh_db_schema
					, max_obj_hist.obh_object_name
			 ) AS max_date
				  ON
					 max_date.obh_db_name = obj_hist.obh_db_name
					 AND max_date.obh_db_schema = obj_hist.obh_db_schema
					 AND max_date.obh_object_name = obj_hist.obh_object_name
					 AND max_date.maximum = obj_hist.obh_object_altered_datetime
		WHERE
			  obj_hist.obh_id IS NULL
			  OR max_date.maximum < all_object.modify_date;



END;
GO
